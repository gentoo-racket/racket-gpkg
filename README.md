# Racket-Gpkg

GPKG support library for Racket.


## About

Gentoo binary package container format support library for the Racket
programming language.

GLEP #78: https://www.gentoo.org/glep/glep-0078.html


## Installation

### Make

Use GNU Make to install Racket-GPKG from its project directory.

```sh
make install
```

### Raco

Use raco to install Racket-GPKG from the official Racket Package Catalog.

```sh
raco pkg install --auto --skip-installed --user gpkg
```

### Req

Use Req to install Racket-GPKG from its project directory.

``` sh
raco req --everything --verbose
```


## Documentation

Documentation can either be built locally with `make public`
or browsed online on either
[GitLab pages](https://gentoo-racket.gitlab.io/racket-gpkg/)
or [Racket-Lang Docs](https://docs.racket-lang.org/gpkg/).


## License

Copyright (c) 2023, Maciej Barć <xgqt@riseup.ne>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
